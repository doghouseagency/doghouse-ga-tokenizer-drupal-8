# GA Tokenizer (D8)

Turns Google analytics cookie values into tokens. 
You can then use these tokens for something like Webform default values in hidden fields.

Most of the code ported from here: https://www.drupal.org/project/ga_tokenizer

The parsing is all done by `Google Analytics PHP cookie parser`.

## What it does

Provides tokens for the following:

* Campaign source
* Campaign name
* Campaign medium
* Campaign content
* Campaign term
* Date of first visit
* Date of previous visit
* Date of current visit
* Times visited

## Requirements

This module utilizes this class: 
https://github.com/the-support-group/Google-Analytics-PHP-cookie-parser

See install instructions below.

## How to install

Install it via composer.

 - Ensure you have the doghouse packages repository in your `composer.json`
```
    "repositories": [
        ...
        {
            "type": "composer",
            "url": "http://packages.doghouse.agency/"
        }
    ],
```

 - In terminal, run: `composer require doghouse/doghouse_ga_tokenizer`
 - Enable the module (EG `drush en doghouse_ga_tokenizer`) or add it as a dependency to your update module

## Once enabled

You should see `Google Analytics tokenizer tokens` tokens available in the `Token Browser`

## Author

- D8: Jeremy Graham
- D7: Current maintainers at https://www.drupal.org/project/ga_tokenizer
